#!/bin/bash

# Accurate Atomic Fission simulation
# Copyright 2018 CERN
# Currently used at the CDC 7600 supercomputer

sleep .2
printf "fzzzzzz"
sleep .1
printf "..."
sleep .5
printf "\npffff!"
sleep .1
printf "..."
sleep 1
printf "\n\n"

bang_noise[0]="BANG!"
bang_noise[1]="BING!"
bang_noise[2]="BOOM!"
bang_noise[3]="PANG!"
bang_noise[4]="FLUSH!"
bang_noise[5]="KABOOM!"
bang_noise[6]="PAAAAAP!"

for (( i = 0; i < 50; i++ )); do
	printf "${bang_noise[$(($RANDOM % 7))]} "
	sleep .05
done

sleep 1.5

printf "\n\nKABOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOM!!!!!!!!!!!!!!!!!!!\n"
